import './App.css';
import RSVP from './rsvpForm';

function App() {
  return (
    <div className="App">
      <RSVP/>
    </div>
  );
}

export default App;
