import React from "react";
import { useState} from "react";

  function RSVP() {
    const [fullName, setFullName] = useState("");
    const [emailAddress , setEmailAddresss] = useState("");
    const [phone, setPhoneNumber] = useState("");

    function handleSubmit(e) {
        e.preventDefault();
    }
  return (
    <div>
        <h1>Mental Health First Aid RSVP</h1>
        <fieldset>
            <form action="./rsvp_firstaid.php" method="post" onSubmit={handleSubmit}>
                <label for="fullName">Full Name</label>
                <input 
                    type='text'
                    namme ="fullName"
                    id="fullName"
                    value={fullName}
                    onChange={(e)=> setFullName(e.target.value)}
                    placeholder='Enter your Full Name'
                    required
                />
                <label>Email</label>
                <input
                    type='text'
                    name='email'
                    id='email'
                    value={emailAddress}
                    onChange={(e)=>setEmailAddresss(e.target.value)}
                    placeholder='Enter your Email Address'
                    required
                />
                <label>Phone</label>
                <input
                    type='text'
                    name='phone'
                    id='phone'
                    value={phone}
                    onChange={(e)=>setPhoneNumber(e.target.value)}
                    placeholder='Enter your Phone Number'
                    required
                />
                <button
                    type="Submit"
                    value="Submit"
                    onClick={(e)=>handleSubmit()}
                >
                    Submit
                </button>
            </form>
        </fieldset>
    </div>
  )
}

export default RSVP;
